To Unibet


Assuming the app will be running on Non-sticky nodes, and quite high traffic,
distrubuted cached Redis is used as well as locks.

- due to the time limit, the code is not polished. ie. no parameter validation, formatter, locks are not well tested. 
- some requirements are not quite understood. ie. "all non-matching pairs of base and target currency of the following currencies should be imported", so I just imported all exchange rates returned from fixer.io
- not enough time to write enough unit tests, some tests are for development purposes, however code are fairly test friendly.


prerequisite:

1. Install Redis and make sure it's running on local machine on it's default port 6379
2. To create the database - select the ExchangeRate.Model project to run update-database (EF Code first)


How to run the app.

1. Open the solution in VS2017
2. Set ExchangeRate.Apis as start up proj and F5 to run the web apis
3. http://localhost:28154/api/exchangerate/get/{baseCurrency}/{targetCurrency}
	ie.  http://localhost:28154/api/exchangerate/get/aud/usd
	
* cache expires in 1 hour, it fetches from fixer and re populate cache everytime when it expires


Thanks
Lucas
