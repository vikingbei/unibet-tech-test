﻿using System.Diagnostics;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using Microsoft.Owin;
using Microsoft.Practices.Unity;
using ExchangeRate.Apis;
using ExchangeRate.Common;
using ExchangeRate.Repositories;
using ExchangeRate.Services;
using Owin;
using StackExchange.Redis;
using Unity.WebApi;

[assembly: OwinStartup(typeof(Startup))]
namespace ExchangeRate.Apis
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration();
            ConnectionMultiplexer redis = ConnectionMultiplexer.Connect("localhost");


            var container = new UnityContainer();
            container.RegisterType<IExchangeRateRepository, ExchangeRateRepository>();
            container.RegisterType<IExchangeRateService, ExchangeRateService>();
            container.RegisterType<IExternalFetchService, ExternalFetchService>();
            container.RegisterType<ICacheManager, CacheManager>(new ContainerControlledLifetimeManager());
            container.RegisterType<IConfigManager, ConfigManager>();
            container.RegisterType<IDatabase>(new ContainerControlledLifetimeManager(), new InjectionFactory(c => redis.GetDatabase()));

            //container.RegisterType<IDatabase>(new InjectionFactory(c=> c.Resolve<ConnectionMultiplexer>().GetDatabase()));
            config.DependencyResolver = new UnityDependencyResolver(container);
            config.Services.Add(typeof(IExceptionLogger), new GlobalExceptionLogger(new TraceSource("MyLogSource", SourceLevels.All)));
            WebApiConfig.Register(config);
            app.UseWebApi(config);
        }

    }


}
