﻿using System.Web.Http;
using ExchangeRate.Services;

namespace ExchangeRate.Apis.Controllers
{
    [RoutePrefix("api/exchangerate")]
    public class ExchangeRateController : ApiController
    {
        private readonly IExchangeRateService _exchangeRateService;

        public ExchangeRateController(IExchangeRateService exchangeRateService)
        {
            _exchangeRateService = exchangeRateService;
        }

        /// <summary>
        /// http://localhost:28154/api/exchangerate/get/aud/usd
        ///
        /// No parameters validation - due to time contraint
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("get/{baseCurrency}/{targetCurrency}")]
        public IHttpActionResult GetExchangeRate(string baseCurrency, string targetCurrency)
        {
            var result = _exchangeRateService.GetExchangeRate(baseCurrency, targetCurrency);
            return Ok(result);
        }

        
    }
}
