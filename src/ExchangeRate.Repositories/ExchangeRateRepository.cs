﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExchangeRate.Models;

namespace ExchangeRate.Repositories
{
    /// <summary>
    /// Didn't bother to create Unit of work + Base Repository for this simple demo
    /// </summary>
    public class ExchangeRateRepository : IExchangeRateRepository
    {
        private RateContext _ctx;

        public ExchangeRateRepository()
        {
            _ctx = new RateContext();
        }
        public void CreateBatch(List<Rate> rates)
        {
            _ctx.Rates.AddRange(rates);
        }

        public List<Rate> GetAll()
        {
            return _ctx.Rates.ToList();
        }

        public void DeleteAll()
        {
            _ctx.Rates.RemoveRange(_ctx.Rates);
        }

        public void Commit()
        {
            _ctx.SaveChanges();
        }
    }
}
