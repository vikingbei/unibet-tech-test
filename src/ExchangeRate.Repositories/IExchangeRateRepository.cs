﻿using System.Collections.Generic;
using System.Linq;
using ExchangeRate.Models;


namespace ExchangeRate.Repositories
{
    public interface IExchangeRateRepository
    {
        void CreateBatch(List<Rate> rates);
        List<Rate> GetAll();
        void DeleteAll();
        void Commit();

    }
    
}
