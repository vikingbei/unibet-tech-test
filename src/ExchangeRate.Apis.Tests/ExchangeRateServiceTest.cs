﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExchangeRate.Common;
using ExchangeRate.Models;
using ExchangeRate.Repositories;
using ExchangeRate.Services;
using StackExchange.Redis;

namespace ExchangeRate.Tests
{
    [TestClass]
    public class ExchangeRateServiceTest
    {
       
        [TestMethod]
        public void ExchangeRateService_GetById()
        {
            var mockFetchService= new Mock<IExternalFetchService>();
            var mockRepo = new Mock<IExchangeRateRepository>();
            var mockCacheManager = new Mock<ICacheManager>();
            var mockConfigManager = new Mock<IConfigManager>();
            var mockDb = new Mock<IDatabase>();

            mockConfigManager.Setup(x => x.AppSettings.CacheExpiryInSeconds).Returns(1);

            var svc = new ExchangeRateService(mockFetchService.Object, mockRepo.Object, mockCacheManager.Object,  mockConfigManager.Object);

            mockCacheManager.VerifyAll();
        }

        [TestMethod]
        public void CalculateRateTest()
        {
            var mockFetchService = new Mock<IExternalFetchService>();
            var mockRepo = new Mock<IExchangeRateRepository>();
            var mockCacheManager = new Mock<ICacheManager>();
            var mockConfigManager = new Mock<IConfigManager>();
            var mockDb = new Mock<IDatabase>();
            var svc = new ExchangeRateService(mockFetchService.Object, mockRepo.Object, mockCacheManager.Object, mockConfigManager.Object);

            var baseCurrency = new Rate {Currency = "AUD", ExchangeRate = 1.536243M, TimeStamp = DateTime.UtcNow};
            var targetCurrency = new Rate { Currency = "CNY", ExchangeRate = 7.511271M, TimeStamp = DateTime.UtcNow };

            var result = svc.CalculateRate(baseCurrency, targetCurrency);

            Assert.AreEqual(result.BaseCurrency, "AUD");
            Assert.AreEqual(result.TargetCurrency, "CNY");
            Assert.AreEqual(result.ExchangeRate, 4.8894M);

        }




    }
}
