﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using ExchangeRate.Common;
using ExchangeRate.Repositories;
using ExchangeRate.Services;

namespace ExchangeRate.Tests
{
    [TestClass]
    public class ExternalFetchServiceTest
    {
        [TestMethod]
        public async Task FetchFixerIoTest()
        {
            var mockRepository = new Mock<IExchangeRateRepository>();
            var mockConfigManager = new Mock<IConfigManager>();
            var mockCacheManager = new Mock<ICacheManager>();

            mockConfigManager.Setup(x => x.AppSettings.ExternalRateSourceUrl).Returns("http://data.fixer.io/api/latest?access_key=659ca9249948ea8cf05d3a9d316631e3");
            var movieService = new ExternalFetchService(mockRepository.Object, mockCacheManager.Object, mockConfigManager.Object);

            var result = await movieService.FetchFixerIo();
            Assert.IsNotNull(result);
        }
        

    }
}
