namespace ExchangeRate.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RunCustomSqlScript : DbMigration
    {
        public override void Up()
        {
            Sql(@"CREATE TRIGGER tr_Rate_Insert ON dbo.Rates
            FOR INSERT AS INSERT INTO RateAudits(RateId, Currency, ExchangeRate, TimeStamp, AuditDate)
            SELECT RateId, Currency, ExchangeRate, TimeStamp, GETDATE() FROM Inserted");


        }

        public override void Down()
        {
            Sql(@"DROP TRIGGER tr_Rate_Insert");
        }
    }
}
