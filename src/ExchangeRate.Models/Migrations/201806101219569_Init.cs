namespace ExchangeRate.Models.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RateAudits",
                c => new
                    {
                        AuditId = c.Int(nullable: false, identity: true),
                        RateId = c.Int(nullable: false),
                        Currency = c.String(nullable: false),
                        ExchangeRate = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TimeStamp = c.DateTime(nullable: false),
                        AuditDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AuditId);
            
            CreateTable(
                "dbo.Rates",
                c => new
                    {
                        RateId = c.Int(nullable: false, identity: true),
                        Currency = c.String(nullable: false),
                        ExchangeRate = c.Decimal(nullable: false, precision: 10, scale: 5),
                        TimeStamp = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.RateId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Rates");
            DropTable("dbo.RateAudits");
        }
    }
}
