﻿using System;

namespace ExchangeRate.Models
{
    public class ExchangeRateResponse
    {
        public string BaseCurrency { get; set; }
        public string TargetCurrency { get; set; }
        
        public decimal ExchangeRate { get; set; }
        public DateTime TimeStamp { get; set; }

    }
    
}
