﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExchangeRate.Models
{
    public class RateAudit
    {
        [Key]
        public int AuditId { get; set; }

        public int RateId { get; set; }
        [Required]
        public string Currency { get; set; }
       
        [Required]
        public decimal ExchangeRate { get; set; }
        [Required]
        public DateTime TimeStamp { get; set; }

        public DateTime AuditDate { get; set; }

    }
    
}
