﻿using System.Data.Entity;

namespace ExchangeRate.Models
{
    public class RateContext : DbContext
    {
        public RateContext() : base("ExchangeRate")
        {
        }

        public DbSet<Rate> Rates { get; set; }
        public DbSet<RateAudit> RateAudits { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Rate>().Property(x => x.ExchangeRate).HasPrecision(10,5);
        }
    }



}
