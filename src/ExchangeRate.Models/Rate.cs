﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ExchangeRate.Models
{
    public class Rate
    {
        [Key]
        public int RateId { get; set; }
        [Required]
        public string Currency { get; set; }
       
        [Required]
        public decimal ExchangeRate { get; set; }
        [Required]
        public DateTime TimeStamp { get; set; }

    }
    
}
