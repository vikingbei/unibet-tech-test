﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ExchangeRate.Common;
using ExchangeRate.Models;


namespace ExchangeRate.Services
{
    public interface IExternalFetchService
    {
        void RefreshExchangeRate();

        Task<string> FetchFixerIo();

        //Task<Dictionary<string, decimal>> FetchExternalSrc();

        void SaveToLocalDb(List<Rate> rates);
        void SaveToCache(List<Rate> rates);
    }

    
}
