﻿using ExchangeRate.Common;
using ExchangeRate.Models;
using ExchangeRate.Repositories;

namespace ExchangeRate.Services
{
    public class ExchangeRateService : IExchangeRateService
    {
        private readonly ICacheManager _cacheManager;
        private readonly IConfigManager _configManager;
        private readonly IExternalFetchService _externalFetchService;
        private readonly IExchangeRateRepository _repo;
        private static object _lock = new object();

        public ExchangeRateService(IExternalFetchService externalFetchService, IExchangeRateRepository repo, ICacheManager cacheManager, IConfigManager configManager)
        {
            _externalFetchService = externalFetchService;
            _repo = repo;
            _cacheManager = cacheManager;
            _configManager = configManager;
        }

        public void RefreshCacheIfExpired(string key)
        {
            if (!_cacheManager.Exists(key))
            {
                lock (_lock)
                {
                    FetchExternalSrc();
                }
            }
        }

        /// <summary>
        /// Get exchange rate from cached, if expired, hit FetchExternalSrc() to pull the latest version and save to local db
        /// </summary>
        /// <param name="baseCurrency"></param>
        /// <param name="targetCurrency"></param>
        /// <returns></returns>
        public ExchangeRateResponse GetExchangeRate(string baseCurrency, string targetCurrency)
        {
            RefreshCacheIfExpired(baseCurrency);
            var baseRate = SearchExchangeRate(baseCurrency);
            var targetRate = SearchExchangeRate(targetCurrency);
            return CalculateRate(baseRate, targetRate);
        }

        public Rate SearchExchangeRate(string currency)
        {
            return _cacheManager.Get(currency);
        }

        /// <summary>
        /// re fetch from fixer.io when cache is expired
        /// </summary>
        /// <returns></returns>
        public void FetchExternalSrc()
        {
             _externalFetchService.RefreshExchangeRate();
        }

        public ExchangeRateResponse CalculateRate(Rate baseCurrency, Rate targetCurrency)
        {
            return new ExchangeRateResponse
            {
                BaseCurrency = baseCurrency.Currency,
                TargetCurrency = targetCurrency.Currency,
                ExchangeRate = (targetCurrency.ExchangeRate / baseCurrency.ExchangeRate).FormatDecimal(),
                TimeStamp = baseCurrency.TimeStamp
            };
        }
    }
}
