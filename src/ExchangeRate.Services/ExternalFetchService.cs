﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using ExchangeRate.Common;
using ExchangeRate.Models;
using ExchangeRate.Repositories;

namespace ExchangeRate.Services
{
    public class ExternalFetchService : IExternalFetchService
    {
        private readonly IExchangeRateRepository _exchangeRateRepository;
        private readonly ICacheManager _cacheManager;
        private readonly IConfigManager _configManager;

        public ExternalFetchService(IExchangeRateRepository exchangeRateRepository, ICacheManager cacheManager, IConfigManager configManager)
        {
            _exchangeRateRepository = exchangeRateRepository;
            _cacheManager = cacheManager;
            _configManager = configManager;
        }
        
        /// <summary>
        /// fetch the latest rate from fixer.io and save it to local db
        /// In the real solution, I'd create a dedicated scheduled service to fetch fixer.io and save into local db.
        /// And the Cache would only grab data from sql db.
        /// 
        /// In this demo, I'm not going to create such seperated windows service. hence merge this 2 function into 1.
        /// </summary>
        /// <returns></returns>
        public void RefreshExchangeRate()
        {
            var fixerJson = Task.Run(async () => await FetchFixerIo());
            var ratesDomain = fixerJson.Result.ToDomain();
            SaveToLocalDb(ratesDomain);
            SaveToCache(ratesDomain);
        }

        public async Task<string> FetchFixerIo()
        {
            using (HttpClient client = new HttpClient())
            {
                return await client.GetStringAsync(_configManager.AppSettings.ExternalRateSourceUrl);
            }
        }
        
        /// <summary>
        /// Not going to implement Unit of Work for this quick demo.
        /// Only use the EF built in transaction for delete and insert.
        /// </summary>
        /// <param name="rates"></param>
        public void SaveToLocalDb(List<Rate> rates)
        {
            _exchangeRateRepository.DeleteAll();
            _exchangeRateRepository.CreateBatch(rates);
            _exchangeRateRepository.Commit();
        }

        public void SaveToCache(List<Rate> rates)
        {
            var ratesFromSql = _exchangeRateRepository.GetAll();
            _cacheManager.Store(ratesFromSql, new TimeSpan(0, 0, _configManager.AppSettings.CacheExpiryInSeconds));
        }
    }
}
