﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ExchangeRate.Common;
using ExchangeRate.Models;


namespace ExchangeRate.Services
{
    public interface IExchangeRateService
    {

        void RefreshCacheIfExpired(string key);
        ExchangeRateResponse GetExchangeRate(string baseCurrency, string targetCurrency);

        Rate SearchExchangeRate(string currency);
        void FetchExternalSrc();

        ExchangeRateResponse CalculateRate(Rate baseCurrency, Rate targetCurrency);
    }
}
