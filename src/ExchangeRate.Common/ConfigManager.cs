﻿using System.Configuration;

namespace ExchangeRate.Common
{
    public class ConfigManager : IConfigManager
    {
        public virtual AppSettings AppSettings
        {
            get { return new AppSettings(); }
        }
    }

    public class AppSettings
    {
        public virtual int CacheExpiryInSeconds
        {
            get
            {
                int secs = 3600; //default to 1 hour
                int.TryParse(ConfigurationManager.AppSettings["CacheExpiryInSeconds"], out secs);
                return secs;
            }
        }

        public virtual string ExternalRateSourceUrl
        {
            get { return ConfigurationManager.AppSettings["ExternalRateSourceUrl"]; }
        }


    }
}
