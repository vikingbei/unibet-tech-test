﻿using System;
using System.Collections.Generic;
using ExchangeRate.Models;
using StackExchange.Redis;

namespace ExchangeRate.Common
{
    public class CacheManager : ICacheManager
    {
        private readonly IDatabase _db;
        private static object _lock = new object();

        public CacheManager(IDatabase db)
        {
            _db = db;
        }
        public bool Exists(string key)
        {
            return _db.KeyExists(key.ToUpper());
        }

        public Rate Get(string key)
        {
            var result = _db.StringGet(key.ToUpper()).ToString();
            return Helper.Deserialize<Rate>(result);
        }

        public void Store(List<Rate> rates, TimeSpan expiry)
        {
            lock (_lock)
            {
                foreach (var rate in rates)
                {
                    _db.StringSet(rate.Currency, rate.Serialize(), expiry);
                }
            }
        }


    }
    
}
