﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExchangeRate.Common
{
    public interface IConfigManager
    {
        AppSettings AppSettings { get; }
    }
}
