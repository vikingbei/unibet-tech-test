﻿using System;
using System.Collections.Generic;
using ExchangeRate.Models;

namespace ExchangeRate.Common
{
    public interface ICacheManager
    {
        bool Exists(string key);
        //decimal Get(string key);
        Rate Get(string key);
        //void Store(string key, string item, TimeSpan expiry);
        void Store(List<Rate> rates, TimeSpan expiry);

        //T GetOrStore<T>(string key, Func<T> item, int expiryInHours);
    }

}
