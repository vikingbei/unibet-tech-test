﻿using System;
using System.Collections.Generic;
using System.Linq;
using ExchangeRate.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Math = System.Math;

namespace ExchangeRate.Common
{
    public static class Mapper
    {
        public static decimal FormatDecimal(this decimal num)
        {
            return Math.Round(num, 4);
        }

        public static List<Rate> ToDomain(this string json)
        {
            var rateJson = JObject.Parse(json).SelectToken("rates");
            var dict = rateJson.ToObject<Dictionary<string, decimal>>();

            var result = from i in dict
                select new Rate
                {
                    Currency = i.Key,
                    ExchangeRate = i.Value,
                    TimeStamp = DateTime.UtcNow
                };
            return result.ToList();
        }
    }

    public static class Helper
    {
        public static string Serialize<T>(this T rates)
        {
            return JsonConvert.SerializeObject(rates);
        }

        public static T Deserialize<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
    }
}
