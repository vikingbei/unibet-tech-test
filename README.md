To Unibet


Assuming the app will be running on Non-sticky nodes, and quite high traffic,
distrubuted cached Redis is used as well as locks.

- due to the time limit, the code is not polished. ie. no parameter validation, formatter, locks are not well tested. 
- some requirements are not quite understood. ie. "all non-matching pairs of base and target currency of the following currencies should be imported", so I just imported all exchange rates returned from fixer.io
- not enough time to write enough unit tests, some tests are for development purposes, however code are fairly test friendly.


prerequisite:

1. Install Redis and make sure it's running on local machine on it's default port 6379
2. To create the database - select the ExchangeRate.Model project to run update-database (EF Code first)


How to run the app.

1. Open the solution in VS2017
2. Set ExchangeRate.Apis as start up proj and F5 to run the web apis
3. http://localhost:28154/api/exchangerate/get/{baseCurrency}/{targetCurrency}
	ie.  http://localhost:28154/api/exchangerate/get/aud/usd
	
* cache expires in 1 hour, it fetches from fixer and re populate cache everytime when it expires

Thanks
Lucas





# Exchange Rate Service

In order for a Bookmaker to see their liabilities in a common currency, An exchange rate microservice is required. This exchange rate service will be responsible for collection and delivery of exchange rate values to internal clients. 

#### It must: ####

- Provide a RESTful api to fetch an exchange rate for a specified _base currency_ and _target currency_;
- Store the given exchange rate in a database, timestamped with audit trail;
- The most recent exchange rate for the requested currency exchange should be returned;
- Converstion rates are to be stored to 5 decimal places;
- Any design decisions and assumptions should be documented.


#### Implementation: ####
The solution must conform to the below requirements:

- C#/ Visiual Studio 2015+;
- Entity Framework 6.x+ / Code First Migrations;
- Leverage a DI Container of your choice;
- Utilize open-source/ free nuget packages/ extensions;
- Provide a RESTful endpoint as described below;
- Retrieve exchange rates from https://fixer.io/


#### Data Source ####
your solution must acquire conversion rates via the REST api provided by fixer.io

all non-matching pairs of _base_ and _target_ currency of the following currencies should be imported:

-        AUD;
-        SEK;
-        USD; 
-        GBP;
-        EUR.


#### REST Api: ####

##### Retrieve Exchange Rate #####

The api must support a GET HTTP verb with the following contract:



```json

{
    "baseCurrency": "GBP",
    "targetCurrency": "AUD"
}

```
where

- baseCurrency : string, required;
- targetCurrency : string, required;


with response:



```json

{
    "baseCurrency": "GBP",
    "targetCurrency": "AUD",
    "exchangeRate" : 0.7192,
    "timestamp" : "2018-03-21T12:13:48.00Z"
}

```

#### NFR's ####

- Expected load on this API is ~5 - 15 requests/ sec;
- Deployed to a load-balanced environment (~2-3 nodes).


#### Submission ####

For a submission to be considered complete it **must**:

 - Demonstrate appropriate use of source control/ versioning;
 - Adhere to modern coding standards/ practices;
 - Be merged to master;
 - Be testable;
 - Solve the problem.

This repository should be forked and shared with _techtest_au@kindredgroup.com_ when complete.

